import flask_whooshalchemy
from flask_admin.contrib.sqla import ModelView
from flask import session
from flask_sqlalchemy import SQLAlchemy

from app import app

db = SQLAlchemy(app)


class Project(db.Model):

    __tablename__ = 'project'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    project_name = db.Column(db.String(100), unique=True)
    employee = db.relationship('User', backref='project', lazy='dynamic')

    def __repr__(self):
        return '%r' % self.project_name


class Designation(db.Model):

    __tablename__ = 'designation'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    designation_name = db.Column(db.String(100), unique=True)
    employee = db.relationship('User', backref='designation', lazy='dynamic')

    def __repr__(self):
        return '%r' % self.designation_name


class Department(db.Model):

    __tablename__ = 'department'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    department_name = db.Column(db.String(100), unique=True)
    employee = db.relationship('User', backref='department', lazy='dynamic')

    def __repr__(self):
        return '%r' % self.department_name


class Role(db.Model):

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(50), unique=True)
    employee = db.relationship('User', backref='role', lazy='dynamic')

    def __repr__(self):
        return '%r' % self.name


class User(db.Model):

    __searchable__ = ['email', 'name', 'company_number', 'employee_number', 'nic']

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    active = db.Column(db.Boolean())
    name = db.Column(db.String(200))
    image_file = db.Column(db.String(20))
    date_of_birth = db.Column(db.Date)
    company_number = db.Column(db.String(6), unique=True)
    employee_number = db.Column(db.String(6))
    nic = db.Column(db.String(12), unique=True)
    contact_number = db.Column(db.String(10))
    emergency_contact_number = db.Column(db.String(10))
    address = db.Column(db.String(200))
    date_of_hire = db.Column(db.Date)
    date_of_join = db.Column(db.Date)
    register_date = db.Column(db.DateTime, default=db.func.current_timestamp())
    role_id = db.Column(db.Integer, db.ForeignKey('role.id'))
    report_to = db.Column(db.Integer, db.ForeignKey('user.id'))
    project_id = db.Column(db.Integer, db.ForeignKey('project.id'))
    department_id = db.Column(db.Integer, db.ForeignKey('department.id'))
    designation_id = db.Column(db.Integer, db.ForeignKey('designation.id'))

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3

    def __repr__(self):
        return '%r' % self.name


flask_whooshalchemy.whoosh_index(app, User)


class LeaveStatus(db.Model):

    __tablename__ = 'leave_status'

    id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.String(50), unique=True)
    leave_history = db.relationship('LeaveHistory', backref='leave_status', lazy='dynamic')
    leave_work = db.relationship('WorkHoliday', backref='leave_status', lazy='dynamic')

    def __repr__(self):
        return '%r' % self.status


class LeaveType(db.Model):

    __tablename__ = 'leave_type'

    id = db.Column(db.Integer, primary_key=True)
    leave_type_name = db.Column(db.String(50), unique=True)
    visibility = db.Column(db.Boolean)
    leave_type = db.relationship('LeaveBalance', backref='leave_type', lazy='dynamic')

    def __repr__(self):
        return '%r' % self.leave_type_name


class LeaveBalance(db.Model):

    __tablename__ = 'leave_balance'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    total = db.Column(db.Float)
    employee_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    leave_type_id = db.Column(db.Integer, db.ForeignKey('leave_type.id'))
    employee = db.relationship('User', backref='leave_balance')


class LeaveHistory(db.Model):

    __tablename__ = 'leave_history'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    applied_date = db.Column(db.DateTime, default=db.func.current_timestamp())
    reason = db.Column(db.String(50))
    disapprove_reason = db.Column(db.String(50))
    approved_date = db.Column(db.DateTime)
    contact_number = db.Column(db.String(10))
    starting_date = db.Column(db.Date)
    number_of_days = db.Column(db.Float)
    acting_arrangements = db.Column(db.String(50))
    approved_person_name = db.Column(db.String(100))

    employee_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    approved_by = db.Column(db.Integer, db.ForeignKey('user.id'))
    leave_status_id = db.Column(db.Integer, db.ForeignKey('leave_status.id'))
    leave_type_id = db.Column(db.Integer, db.ForeignKey('leave_type.id'))


class WorkHoliday(db.Model):

    __tablename__ = 'work_holiday'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    date = db.Column(db.Date)
    applied_date = db.Column(db.DateTime, default=db.func.current_timestamp())
    approved_date = db.Column(db.DateTime)
    disapprove_reason = db.Column(db.String(50))
    approved_person_name = db.Column(db.String(100))
    leave_status_id = db.Column(db.Integer, db.ForeignKey('leave_status.id'))
    employee_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    approved_by = db.Column(db.Integer, db.ForeignKey('user.id'))
    leave_type_id = db.Column(db.Integer, db.ForeignKey('leave_type.id'))


class Month(db.Model):
    month_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    month = db.Column(db.String(20))

    def __repr__(self):
        return '%r' % self.month


class Year(db.Model):
    year_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    year = db.Column(db.String(4))

    def __repr__(self):
        return '%r' % self.year


class Salary(db.Model):
    salary_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    month_fk = db.Column(db.Integer, db.ForeignKey('month.month_id'))
    year_fk = db.Column(db.Integer, db.ForeignKey('year.year_id'))
    user_fk = db.Column(db.Integer, db.ForeignKey('user.id'))
    pdf = db.Column(db.String(20))
    generate_date = db.Column(db.DateTime, default=db.func.current_timestamp())
    employee = db.relationship('User', backref='salary')
    year = db.relationship('Year', backref='salary')
    month = db.relationship('Month', backref='salary')


class AdminView(ModelView):
    def is_accessible(self):
        if session['role'] == 'admin' or session['role'] == 'super':
            return True
        else:
            return False


class SuperView(ModelView):
    def is_accessible(self):
        if session['role'] == 'super':
            return True
        else:
            return False


class EmployeeView(ModelView):
    excluded_list_columns = 'password'
    can_create = False
    searchable_columns = ('name', 'nic', 'email', 'company_number', 'employee_number')

    def is_accessible(self):
        if session['role'] == 'admin' or session['role'] == 'super':
            return True
        else:
            return False


