from flask import Blueprint, render_template, redirect, url_for, send_from_directory
from flask_login import login_required, current_user
from app import app

from flask.helpers import flash
import model

salaryAPI = Blueprint('salaryAPI', __name__, template_folder='templates/salary')


@salaryAPI.route('/salary/<year>/<month>/<pdf>', methods=['GET'])
@login_required
def salary_view(year, month, pdf):

    year = model.Year.query.filter_by(year=year).first()
    month = model.Month.query.filter_by(month=month).first()

    salary_user = model.Salary.query.\
        join(model.Year, model.Year.year_id == model.Salary.year_fk). \
        join(model.Month, model.Month.month_id == model.Salary.month_fk). \
        add_columns(model.Year.year, model.Month.month, model.Salary.user_fk, model.Salary.pdf). \
        filter(model.Salary.user_fk == current_user.id).\
        filter(model.Salary.year_fk == year.year_id).\
        filter(model.Salary.month_fk == month.month_id).\
        filter(model.Salary.pdf == pdf). \
        first()

    if salary_user is None:
        flash('Invalid', 'warning')
        return redirect(url_for('userAPI.index'))
    else:
        url = salary_user.year + '/' + salary_user.month + '/' + salary_user.pdf
        return send_from_directory(app.config['CUSTOM_STATIC_PATH'], url)


@salaryAPI.route('/salary', methods=['GET'])
@login_required
def salary():

    salary_user = model.Salary.query.\
        join(model.Year, model.Year.year_id == model.Salary.year_fk). \
        join(model.Month, model.Month.month_id == model.Salary.month_fk). \
        add_columns(model.Year.year, model.Month.month, model.Salary.user_fk, model.Salary.pdf). \
        filter(model.Salary.user_fk == current_user.id)

    return render_template('salary/salary.html', salary_user=salary_user)
