from flask import Blueprint, render_template, request, redirect, url_for, jsonify, json
from datetime import datetime
from flask_login import login_required, current_user

from flask.helpers import flash
from functions import required_roles, email
from form import ApplyLeave, WorkHoliday
import model

leaveAPI = Blueprint('leaveAPI', __name__, template_folder='templates/leave')


@leaveAPI.route('/leave', methods=['POST', 'GET'])
@login_required
def leave():
    form = ApplyLeave(request.form)

    leave_balance = model.LeaveBalance.query.\
        join(model.LeaveType, model.LeaveBalance.leave_type_id == model.LeaveType.id).\
        add_columns(model.LeaveType.id, model.LeaveType.leave_type_name, model.LeaveType.visibility,
                    model.LeaveBalance.total).\
        filter(model.LeaveBalance.employee_id == current_user.id)
    report = model.User.query.filter(model.User.id == current_user.report_to).first()

    if request.method == 'POST' and form.validate():
        reason = form.reason.data
        contact_number = form.contact_number.data
        acting_arrangements = form.acting_arrangements.data
        starting_date = form.start_date.data
        number_of_days = form.number_of_days.data
        leave_type_id = request.form['leave_type']

        leave_max = None
        for temp in leave_balance:
            if int(temp.id) == int(leave_type_id):
                leave_max = temp.total
                break

        if number_of_days <= leave_max:
            reporter = report.id
            leave_history = model.LeaveHistory(reason=reason, contact_number=contact_number,
                                               starting_date=starting_date, number_of_days=number_of_days,
                                               acting_arrangements=acting_arrangements, employee_id=current_user.id,
                                               approved_by=reporter, leave_status_id=1, leave_type_id=leave_type_id)
            model.db.session.add(leave_history)
            model.db.session.flush()

            link = 'http://192.168.61.74/leave/' + str(leave_history.id)

            report_to = model.User.query.filter_by(id=reporter).first()

            subject = 'Leave Approval'
            body = 'Please review the leave from  ' + current_user.name
            html = render_template('leave/email_approval.html', link=link, report_to=report_to, subject=subject,
                                   body=body)

            model.db.session.commit()

            if email(subject, report_to.email, html):
                flash('Leave send for approval', 'success')
                return redirect(url_for('leaveAPI.leave_history'))
            else:
                flash('Request has a problem', 'warning')

        else:
            flash('You do not have sufficient leaves', 'warning')
            return redirect(url_for('leaveAPI.leave'))

    reporters = ''
    if current_user.role_id == 4:
        reporters = model.User.query.filter(model.User.designation_id.in_([35, 36])).\
            filter(model.User.id != current_user.id)
    elif current_user.department_id == 6 and current_user.role_id == 4:
        reporters = model.User.query.filter(model.User.designation_id.in_([14, 16])).\
            filter(model.User.id != current_user.id)
    return render_template('leave/leave.html', form=form, leave_balance=leave_balance, reporters=reporters,
                           report=report)


@leaveAPI.route('/leave/history', methods=['GET'])
@login_required
def leave_history():

    leave_history = model.LeaveHistory.query.\
        join(model.LeaveType, model.LeaveHistory.leave_type_id == model.LeaveType.id).\
        add_columns(model.LeaveType.leave_type_name, model.LeaveHistory.applied_date, model.LeaveHistory.reason,
                    model.LeaveHistory.contact_number, model.LeaveHistory.starting_date,
                    model.LeaveHistory.number_of_days, model.LeaveHistory.acting_arrangements,
                    model.LeaveHistory.leave_status_id, model.LeaveHistory.id,
                    model.LeaveHistory.approved_person_name, model.LeaveHistory.disapprove_reason).\
        filter(model.LeaveHistory.employee_id == current_user.id)

    return render_template('leave/leave_history.html', leave_history=leave_history)


@leaveAPI.route('/leave/<int:leave_id>', methods=['GET'])
@login_required
@required_roles('super', 'admin', 'staff')
def leave_approval(leave_id):
    leave_history = model.LeaveHistory.query. \
        join(model.LeaveType, model.LeaveHistory.leave_type_id == model.LeaveType.id). \
        join(model.LeaveStatus, model.LeaveStatus.id == model.LeaveHistory.leave_status_id). \
        join(model.User, model.User.id == model.LeaveHistory.employee_id). \
        join(model.Designation, model.User.designation_id == model.Designation.id). \
        add_columns(model.LeaveType.leave_type_name, model.LeaveHistory.applied_date, model.User.id,
                    model.LeaveHistory.reason, model.LeaveHistory.starting_date, model.LeaveHistory.number_of_days,
                    model.LeaveHistory.acting_arrangements, model.LeaveHistory.leave_status_id,
                    model.LeaveStatus.status, model.LeaveHistory.leave_status_id, model.User.name,
                    model.Designation.designation_name, model.LeaveHistory.approved_person_name,
                    model.User.company_number, model.LeaveHistory.disapprove_reason). \
        filter(model.LeaveHistory.id == leave_id).first()

    if leave_history is None:
        flash('Expired', 'warning')

    return render_template('leave/leave_approval.html', leave_history=leave_history, leave_id=leave_id)


@leaveAPI.route('/leave/delete/<int:leave_id>', methods=['GET'])
@login_required
def delete(leave_id):
    leave_history = model.LeaveHistory.query.filter_by(id=leave_id).filter_by(employee_id=current_user.id).first()

    if leave_history.leave_status_id == 2:
        leave_balance = model.LeaveBalance.query.\
            filter_by(leave_type_id=leave_history.leave_type_id).\
            filter_by(employee_id=current_user.id).first()
        leave_balance.total = leave_balance.total + leave_history.number_of_days
        model.db.session.commit()

    leave_history.leave_status_id = 4
    model.db.session.commit()

    flash('Leave deleted', 'warning')
    return redirect(url_for('leaveAPI.leave_history'))


@leaveAPI.route('/leave/leave_review', methods=['GET'])
@login_required
@required_roles('super', 'admin', 'staff')
def leave_review():
    leave_history = model.LeaveHistory.query.\
        join(model.LeaveType, model.LeaveHistory.leave_type_id == model.LeaveType.id). \
        join(model.User, model.LeaveHistory.employee_id == model.User.id). \
        add_columns(model.LeaveType.leave_type_name, model.LeaveHistory.applied_date, model.LeaveHistory.reason,
                    model.LeaveHistory.contact_number, model.LeaveHistory.starting_date,
                    model.LeaveHistory.number_of_days, model.LeaveHistory.acting_arrangements,
                    model.LeaveHistory.leave_status_id, model.LeaveHistory.id, model.User.name,
                    model.LeaveHistory.disapprove_reason).\
        filter(model.LeaveHistory.approved_by == current_user.id)

    return render_template('leave/leave_review.html', leave_history=leave_history)


@leaveAPI.route('/leave/approval', methods=['POST'])
@login_required
@required_roles('super', 'admin', 'staff')
def leave_post_approval():
    data = json.loads(request.data)
    leave_id = data['leave_id']
    answer_id = data['answer_id']

    leave_history = model.LeaveHistory.query.\
        filter(model.LeaveHistory.id == leave_id).first()

    leave_history.approved_date = datetime.now()
    leave_history.approved_person_name = current_user.name

    if answer_id == 2:
        leave_history.leave_status_id = answer_id
        model.db.session.commit()
        leave_balance = model.LeaveBalance.query.\
            filter_by(employee_id=leave_history.employee_id).\
            filter_by(leave_type_id=leave_history.leave_type_id).first()
        leave_balance.total = leave_balance.total - leave_history.number_of_days
        model.db.session.commit()

        flash('Leave approved', 'success')

    elif answer_id == 3:
        if leave_history.leave_status_id == 2:
            leave_balance = model.LeaveBalance.query. \
                filter_by(employee_id=leave_history.employee_id). \
                filter_by(leave_type_id=leave_history.leave_type_id).first()
            leave_balance.total = leave_balance.total + 1
            model.db.session.commit()

        leave_history.disapprove_reason = data['reason']
        leave_history.leave_status_id = answer_id
        model.db.session.commit()
        flash('Leave disapproved', 'warning')

    if current_user.role_id == 1 or current_user.role_id == 2:
        return jsonify({'redirect': url_for('dashboardAPI.leaves_all')})
    else:
        return jsonify({'redirect': url_for('leaveAPI.leave_review')})


@leaveAPI.route('/leave/approval_intermediate', methods=['POST'])
@login_required
def leave_post_approval_intermediate():
    data = json.loads(request.data)
    leave_id = data['leave_id']
    answer_id = data['answer_id']

    leave_history = model.LeaveHistory.query.\
        filter(model.LeaveHistory.id == leave_id).first()

    leave_history.approved_date = datetime.now()
    leave_history.approved_person_name = current_user.name

    if answer_id == 5:
        leave_history.leave_status_id = answer_id
        leave_history.approved_by = data['supervisor_id']
        model.db.session.commit()

        link = 'http://192.168.61.74/leave/' + str(leave_history.id)

        report_to = model.User.query.filter_by(id=data['supervisor_id']).first()

        subject = 'Leave Approval'
        body = 'Please review the leave'
        html = render_template('leave/email_approval.html', link=link, report_to=report_to, subject=subject, body=body)

        if email(subject, report_to.email, html):
            flash('Leave send to supervisor approval', 'success')
        else:
            flash('Request has a problem', 'warning')

    elif answer_id == 3:
        if leave_history.leave_status_id == 2:
            leave_balance = model.LeaveBalance.query. \
                filter_by(employee_id=leave_history.employee_id). \
                filter_by(leave_type_id=leave_history.leave_type_id).first()
            leave_balance.total = leave_balance.total + 1
            model.db.session.commit()

        leave_history.disapprove_reason = data['reason']
        leave_history.leave_status_id = answer_id
        model.db.session.commit()
        flash('Leave disapproved', 'warning')

    return jsonify({'redirect': url_for('leaveAPI.leave_review')})


@leaveAPI.route('/work', methods=['POST', 'GET'])
@login_required
def work():
    form = WorkHoliday(request.form)

    reporters = model.User.query.filter(model.User.designation_id.in_([35, 36])).filter(model.User.id != current_user.id)
    report = model.User.query.filter(model.User.id == current_user.report_to).first()

    if request.method == 'POST' and form.validate():
        date = form.date.data
        leave_history = model.WorkHoliday(date=date, employee_id=current_user.id, approved_by=current_user.report_to,
                                          leave_status_id=1, leave_type_id=6)
        report_to = model.User.query.filter_by(id=current_user.report_to).first()
        model.db.session.add(leave_history)
        model.db.session.commit()
        link = 'http://192.168.61.74/work/' + str(leave_history.id)

        subject = 'Approval Work on Holiday'
        body = 'Please review work on holiday request'
        html = render_template('leave/email_approval.html', link=link, report_to=report_to, subject=subject, body=body)

        if email(subject, report_to.email, html):
            flash('Work on holiday send for approval', 'success')
            return redirect(url_for('leaveAPI.work_history'))
        else:
            flash('Request has a problem', 'warning')

    return render_template('leave/work.html', form=form, reporters=reporters, report=report)


@leaveAPI.route('/work/history', methods=['POST', 'GET'])
@login_required
def work_history():
    leave_balance = model.WorkHoliday.query.\
        add_columns(model.WorkHoliday.date, model.WorkHoliday.applied_date, model.WorkHoliday.approved_date,
                    model.WorkHoliday.approved_person_name, model.WorkHoliday.leave_status_id,
                    model.WorkHoliday.id, model.WorkHoliday.disapprove_reason). \
        filter(model.WorkHoliday.employee_id == current_user.id)

    return render_template('leave/work_history.html', leave_balance=leave_balance)


@leaveAPI.route('/work_intermediate/<int:leave_id>', methods=['GET'])
@login_required
@required_roles('super', 'admin', 'staff')
def work_intermediate(leave_id):
    leave_history = model.WorkHoliday.query. \
        join(model.LeaveStatus, model.LeaveStatus.id == model.WorkHoliday.leave_status_id). \
        join(model.User, model.User.id == model.WorkHoliday.employee_id). \
        join(model.Designation, model.User.designation_id == model.Designation.id). \
        add_columns(model.WorkHoliday.applied_date, model.WorkHoliday.date, model.WorkHoliday.leave_status_id,
                    model.LeaveStatus.status, model.User.name, model.User.company_number, model.User.id,
                    model.Designation.designation_name, model.WorkHoliday.approved_person_name). \
        filter(model.WorkHoliday.id == leave_id).first()

    if leave_history is None:
        flash('Expired', 'warning')

    reporters = model.User.query.filter(model.User.designation_id.in_([24, 23, 22])). \
        filter(model.User.id != current_user.id)

    return render_template('leave/work_intermediate.html', leave_history=leave_history, leave_id=leave_id, reporters=reporters)


@leaveAPI.route('/work/<int:leave_id>', methods=['GET'])
@login_required
@required_roles('super', 'admin', 'staff')
def work_approval(leave_id):
    leave_history = model.WorkHoliday.query. \
        join(model.LeaveStatus, model.LeaveStatus.id == model.WorkHoliday.leave_status_id). \
        join(model.User, model.User.id == model.WorkHoliday.employee_id). \
        join(model.Designation, model.User.designation_id == model.Designation.id). \
        add_columns(model.WorkHoliday.applied_date, model.WorkHoliday.date, model.WorkHoliday.leave_status_id,
                    model.LeaveStatus.status, model.User.name, model.User.company_number, model.User.id,
                    model.Designation.designation_name, model.WorkHoliday.approved_person_name,
                    model.WorkHoliday.disapprove_reason). \
        filter(model.WorkHoliday.id == leave_id).first()

    if leave_history is None:
        flash('Expired', 'warning')

    return render_template('leave/work_approval.html', leave_history=leave_history, leave_id=leave_id)


@leaveAPI.route('/work/delete/<int:leave_id>', methods=['GET'])
@login_required
def work_delete(leave_id):
    leave_history = model.WorkHoliday.query.filter_by(id=leave_id).filter_by(employee_id=current_user.id).first()

    if leave_history is None:
        flash('Expired', 'warning')
        return redirect(url_for('leaveAPI.work'))
    else:
        if leave_history.leave_status_id == 2:
            leave_balance = model.LeaveBalance.query.\
                filter_by(leave_type_id=leave_history.leave_type_id).\
                filter_by(employee_id=current_user.id).first()
            leave_balance.total = leave_balance.total - 1
            model.db.session.commit()

        leave_history.leave_status_id = 4
        model.db.session.commit()

        flash('Leave deleted', 'warning')
        return redirect(url_for('leaveAPI.work_history'))


@leaveAPI.route('/work/work_review', methods=['GET'])
@login_required
@required_roles('super', 'admin', 'staff')
def work_review():
    work_history = model.WorkHoliday.query. \
        join(model.LeaveType, model.WorkHoliday.leave_type_id == model.LeaveType.id). \
        join(model.User, model.WorkHoliday.employee_id == model.User.id). \
        add_columns(model.LeaveType.leave_type_name, model.WorkHoliday.applied_date, model.WorkHoliday.date,
                    model.WorkHoliday.leave_status_id, model.User.name, model.WorkHoliday.id,
                    model.WorkHoliday.disapprove_reason). \
        filter(model.WorkHoliday.approved_by == current_user.id)

    return render_template('leave/work_review.html', work_history=work_history)


@leaveAPI.route('/work/approval', methods=['POST'])
@login_required
@required_roles('super', 'admin', 'staff')
def work_post_approval():
    data = json.loads(request.data)
    leave_id = data['leave_id']
    answer_id = data['answer_id']

    leave_history = model.WorkHoliday.query. \
        filter(model.WorkHoliday.id == leave_id).first()

    leave_history.approved_date = datetime.now()
    leave_history.approved_person_name = current_user.name

    if answer_id == 2:
        leave_history.leave_status_id = answer_id
        model.db.session.commit()

        leave_balance = model.LeaveBalance.query. \
            filter_by(employee_id=leave_history.employee_id). \
            filter_by(leave_type_id=6).first()
        leave_balance.total = leave_balance.total + 1
        model.db.session.commit()

        flash('Work on Holiday approved', 'success')

    elif answer_id == 3:
        if leave_history.leave_status_id == 2:
            leave_balance = model.LeaveBalance.query. \
                filter_by(employee_id=leave_history.employee_id). \
                filter_by(leave_type_id=6).first()
            leave_balance.total = leave_balance.total + 1
            model.db.session.commit()

        leave_history.disapprove_reason = data['reason']
        leave_history.leave_status_id = answer_id
        model.db.session.commit()

        flash('Work on Holiday disapproved', 'warning')

    if current_user.role_id == 1 or current_user.role_id == 2:
        return jsonify({'redirect': url_for('dashboardAPI.work_all')})
    else:
        return jsonify({'redirect': url_for('leaveAPI.work_review')})


@leaveAPI.route('/leave/update/<int:user_id>', methods=['POST', 'GET'])
@login_required
@required_roles('super', 'admin')
def edit_leave(user_id):
    leave_balance = model.LeaveBalance.query. \
                    join(model.LeaveType, model.LeaveBalance.leave_type_id == model.LeaveType.id). \
                    add_columns(model.LeaveBalance.employee_id, model.LeaveType.id, model.LeaveType.leave_type_name,
                                model.LeaveType.visibility, model.LeaveBalance.leave_type_id,
                                model.LeaveBalance.total). \
                    filter(model.LeaveBalance.employee_id == user_id)

    if request.method == 'POST':
        count = 0
        for leave in leave_balance:
            if count == 0:
                temp = model.LeaveBalance.query.filter(model.LeaveBalance.employee_id == leave.employee_id).\
                    filter(model.LeaveBalance.leave_type_id == leave.leave_type_id).first()
                temp.total = request.form['Annual']
                model.db.session.commit()
            elif count == 1:
                temp = model.LeaveBalance.query.filter(model.LeaveBalance.employee_id == leave.employee_id).\
                    filter(model.LeaveBalance.leave_type_id == leave.leave_type_id).first()
                temp.total = request.form['Medical']
                model.db.session.commit()
            elif count == 2:
                temp = model.LeaveBalance.query.filter(model.LeaveBalance.employee_id == leave.employee_id).\
                    filter(model.LeaveBalance.leave_type_id == leave.leave_type_id).first()
                temp.total = request.form['Casual']
                model.db.session.commit()
            elif count == 3:
                temp = model.LeaveBalance.query.filter(model.LeaveBalance.employee_id == leave.employee_id).\
                    filter(model.LeaveBalance.leave_type_id == leave.leave_type_id).first()
                temp.total = request.form['NoPay']
                model.db.session.commit()
            elif count == 4:
                temp = model.LeaveBalance.query.filter(model.LeaveBalance.employee_id == leave.employee_id).\
                    filter(model.LeaveBalance.leave_type_id == leave.leave_type_id).first()
                temp.total = request.form['Maternity']
                model.db.session.commit()
            elif count == 5:
                temp = model.LeaveBalance.query.filter(model.LeaveBalance.employee_id == leave.employee_id).\
                    filter(model.LeaveBalance.leave_type_id == leave.leave_type_id).first()
                temp.total = request.form['Leave']
                model.db.session.commit()

            count += 1

        flash('Leave Updated', 'success')
        return redirect(url_for('userAPI.profile_id', user_id=user_id))

    return render_template('leave/edit_leave.html', leave_balance=leave_balance)