from flask import Blueprint, render_template, session, redirect, url_for, request, send_from_directory
from flask_login import login_required, login_user, logout_user, current_user
from passlib.hash import sha256_crypt
import os

import model
from app import app
from flask.helpers import flash
from form import ChangePassword, RegisterEmployee
from functions import required_roles

userAPI = Blueprint('userAPI', __name__)

ALLOWED_EXTENSIONS = ['png', 'jpg', 'jpeg']


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@userAPI.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():

    employee = model.User.query. \
        join(model.Project, model.Project.id == model.User.project_id). \
        join(model.Designation, model.Designation.id == model.User.designation_id). \
        join(model.Department, model.Department.id == model.User.department_id). \
        add_columns(model.User.email, model.User.name, model.User.date_of_birth, model.User.report_to,
                    model.User.company_number, model.User.employee_number, model.User.nic, model.User.image_file,
                    model.User.contact_number, model.User.emergency_contact_number, model.User.address,
                    model.User.date_of_hire, model.User.date_of_join, model.User.register_date,
                    model.Project.project_name, model.Department.department_name,
                    model.Designation.designation_name). \
        filter(model.User.id == current_user.id).first()
    report_to = model.User.query.filter_by(id=employee.report_to).first()

    user = model.User.query.filter_by(id=current_user.id).first()

    if request.method == 'POST':
        file = request.files['image']

        if file and allowed_file(file.filename):
            file.filename = user.employee_number + '.' + file.filename.rsplit('.', 1)[1]
            f = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
            user.image_file = file.filename
            model.db.session.commit()
            file.save(f)

            flash('Profile Updated, press Ctrl + F5 to view the update', 'success')
            return redirect(url_for('userAPI.profile'))

    return render_template('profile/profile.html', employee=employee, report_to=report_to)


@userAPI.route('/image/<image_id>', methods=['GET', 'POST'])
@login_required
def image(image_id):
    user = model.User.query.filter_by(id=image_id).first()

    return send_from_directory(app.config['UPLOAD_FOLDER'], user.image_file)


@userAPI.route('/profile/edit', methods=['GET', 'POST'])
@login_required
def profile_edit():
    employee = model.User.query.filter_by(id=session['id']).first()

    form = RegisterEmployee(obj=employee)

    if request.method == 'POST' and form.validate():
        form = RegisterEmployee(request.form)
        contact_number = form.contact_number.data
        emergency_contact_number = form.emergency_contact_number.data
        address = form.address.data

        employee.contact_number = contact_number
        employee.emergency_contact_number = emergency_contact_number
        employee.address = address
        model.db.session.commit()

        flash('Profile Updated', 'success')
        return redirect(url_for('userAPI.profile'))

    return render_template('profile/edit.html', form=form)


@userAPI.route('/profile/<int:user_id>', methods=['GET'])
@login_required
@required_roles('super', 'admin', 'executive')
def profile_id(user_id):

    employee = model.User.query. \
        join(model.Project, model.Project.id == model.User.project_id). \
        join(model.Designation, model.Designation.id == model.User.designation_id). \
        join(model.Department, model.Department.id == model.User.department_id). \
        add_columns(model.User.id, model.User.email, model.User.name, model.User.date_of_birth, model.User.report_to,
                    model.User.company_number, model.User.employee_number, model.User.nic, model.User.image_file,
                    model.User.contact_number, model.User.emergency_contact_number, model.User.address,
                    model.User.date_of_hire, model.User.date_of_join, model.User.register_date,
                    model.User.role_id, model.Project.project_name, model.Department.department_name,
                    model.Designation.designation_name).\
        filter(model.User.id == user_id).first()
    reporter = model.User.query.filter(model.User.id == employee.report_to).first()

    leave_balance = model.LeaveBalance.query.\
        join(model.LeaveType, model.LeaveBalance.leave_type_id == model.LeaveType.id).\
        add_columns(model.LeaveType.id, model.LeaveType.leave_type_name, model.LeaveType.visibility, model.LeaveBalance.total).\
        filter(model.LeaveBalance.employee_id == user_id)

    return render_template('dashboard/profile_view.html', employee=employee, reporter=reporter, leave_balance=leave_balance)


@userAPI.route('/')
def index():
    return render_template('index.html')


@userAPI.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']

        user = model.User.query.filter_by(email=email).first()

        if user is not None:
            if sha256_crypt.verify(password, user.password):
                session['logged_in'] = True
                session['id'] = user.id

                if user.role_id == 1:
                    session['role'] = 'super'
                elif user.role_id == 2:
                    session['role'] = 'admin'
                elif user.role_id == 3:
                    session['role'] = 'staff'
                elif user.role_id == 4:
                    session['role'] = 'employee'

                login_user(user)

                flash('You are logged in', 'success')

                if 'next' in session:
                    return redirect(session['next'])

                return redirect(url_for('userAPI.index'))
            else:
                error = 'Invalid login'
                return render_template('login.html', error=error)

        else:
            error = 'Email does not Exit'
            return render_template('login.html', error=error)

    return render_template('login.html')


@userAPI.route('/logout')
@login_required
def logout():
    session.clear()
    logout_user()

    flash('You are logged out', 'success')
    return redirect(url_for('userAPI.login'))


@userAPI.route('/password', methods=['GET', 'POST'])
@login_required
def change_password():

    form = ChangePassword(request.form)

    user = model.User.query.filter_by(id=session['id']).first()

    if request.method == 'POST' and form.validate():
        if sha256_crypt.verify(form.current_password.data, user.password):
            user.password = sha256_crypt.encrypt(str(form.new_password.data))
            model.db.session.commit()

        flash('Password changed', 'success')
        return redirect(url_for('userAPI.profile'))

    return render_template('profile/password.html', form=form)
