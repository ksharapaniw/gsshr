from flask import Blueprint, render_template, request, redirect, url_for, session
from passlib.hash import sha256_crypt
from flask_login import login_required, current_user

from flask.helpers import flash
from sqlalchemy.exc import IntegrityError

import model
from functions import required_roles
from form import AddProject, AddDepartment, AddDesignation, RegisterEmployee, Report
from app import excel

dashboardAPI = Blueprint('dashboardAPI', __name__, template_folder='templates/dashboard')


@dashboardAPI.route('/dashboard')
@login_required
@required_roles('super', 'admin', 'executive')
def dashboard():

    return render_template('dashboard.html')


@dashboardAPI.route('/dashboard/projects', methods=['GET', 'POST'])
@login_required
@required_roles('super', 'admin')
def projects():
    project_set = model.Project.query.all()

    form = AddProject(request.form)

    if request.method == 'POST' and form.validate():
        project_name = form.project_name.data

        try:
            data = model.Project(project_name=project_name)
            model.db.session.add(data)
            model.db.session.commit()

            flash('Project added', 'success')
            return redirect(url_for('dashboardAPI.projects'))
        except IntegrityError:
            flash('Duplicate Entity', 'warning')
            return redirect(url_for('dashboardAPI.projects'))

    return render_template('dashboard/projects.html', form=form, project_set=project_set)


@dashboardAPI.route('/dashboard/departments', methods=['GET', 'POST'])
@login_required
@required_roles('super', 'admin')
def departments():
    department_set = model.Department.query.all()

    form = AddDepartment(request.form)

    if request.method == 'POST' and form.validate():
        department_name = form.department_name.data

        try:
            data = model.Department(department_name=department_name)
            model.db.session.add(data)
            model.db.session.commit()

            flash('Department added', 'success')
            return redirect(url_for('dashboardAPI.departments'))
        except IntegrityError:
            flash('Duplicate Entity', 'warning')
            return redirect(url_for('dashboardAPI.departments'))

    return render_template('dashboard/departments.html', form=form, department_set=department_set)


@dashboardAPI.route('/dashboard/designations', methods=['GET', 'POST'])
@login_required
@required_roles('super', 'admin')
def designations():
    designation_set = model.Designation.query.all()

    form = AddDesignation(request.form)

    if request.method == 'POST' and form.validate():
        designation_name = form.designation_name.data

        try:
            data = model.Designation(designation_name=designation_name)
            model.db.session.add(data)
            model.db.session.commit()

            flash('Designation added', 'success')
            return redirect(url_for('dashboardAPI.designations'))
        except IntegrityError:
            flash('Duplicate Entity', 'warning')
            return redirect(url_for('dashboardAPI.designations'))

    return render_template('dashboard/designations.html', form=form, designation_set=designation_set)


@dashboardAPI.route('/dashboard/add_member', methods=['GET', 'POST'])
@login_required
@required_roles('super', 'admin')
def add_member():
    project_count = model.Project.query.all()
    department_count = model.Department.query.all()
    designation_count = model.Designation.query.all()
    employee_count = model.User.query.filter(model.User.role_id.in_([1, 2, 3]))

    form = RegisterEmployee(request.form)

    if request.method == 'POST' and form.validate():
        name = form.name.data
        email = form.email.data
        date_of_birth = form.date_of_birth.data
        company_number = form.company_number.data
        employee_number = form.employee_number.data
        nic = form.nic.data
        contact_number = form.contact_number.data
        emergency_contact_number = form.emergency_contact_number.data
        address = form.address.data
        date_of_hire = form.date_of_hire.data
        date_of_join = form.date_of_join.data
        project_id = request.form['project']
        report_to = request.form['employee']
        department_id = request.form['department']
        designation_id = request.form['designation']
        password = sha256_crypt.encrypt(str(nic))
        role_id = request.form['role']

        try:
            employee = model.User(name=name, email=email, date_of_birth=date_of_birth, company_number=company_number,
                                  employee_number=employee_number, nic=nic, contact_number=contact_number,
                                  emergency_contact_number=emergency_contact_number, address=address,
                                  date_of_hire=date_of_hire, date_of_join=date_of_join, project_id=project_id,
                                  report_to=report_to, department_id=department_id, designation_id=designation_id,
                                  password=password, role_id=role_id, image_file='image.png')

            model.db.session.add(employee)
            model.db.session.flush()

            model.db.session.commit()

            model.db.session.add_all([
                model.LeaveBalance(total=14, employee_id=employee.id, leave_type_id=1),
                model.LeaveBalance(total=0, employee_id=employee.id, leave_type_id=2),
                model.LeaveBalance(total=7, employee_id=employee.id, leave_type_id=3),
                model.LeaveBalance(total=0, employee_id=employee.id, leave_type_id=4),
                model.LeaveBalance(total=0, employee_id=employee.id, leave_type_id=5),
                model.LeaveBalance(total=0, employee_id=employee.id, leave_type_id=6)
            ])
            model.db.session.commit()

            flash('Employee added', 'success')
            return redirect(url_for('dashboardAPI.add_member'))

        except IntegrityError:
            flash('Duplicate Entity', 'warning')
            return redirect(url_for('dashboardAPI.add_member'))

    return render_template('dashboard/add_member.html', form=form, project_count=project_count,
                           department_count=department_count, designation_count=designation_count,
                           employee_count=employee_count)


@dashboardAPI.route('/dashboard/edit_member/<int:user_id>', methods=['GET', 'POST'])
@login_required
@required_roles('super', 'admin')
def edit_member(user_id):

    employee_count = model.User.query.filter(model.User.role_id == 5).filter(model.User.id != current_user.id)
    update_user = model.User.query.get(user_id)

    if request.method == 'POST':
        report_to = request.form['employee']

        update_user.report_to = report_to
        model.db.session.commit()

        flash('Employee Updated', 'success')
        return redirect(url_for('userAPI.profile_id', user_id=user_id))

    return render_template('dashboard/edit_member.html', employee_count=employee_count)


@dashboardAPI.route('/dashboard/team', methods=['GET', 'POST'])
@login_required
@required_roles('super', 'admin', 'staff')
def team():
    member_set = model.User.query.\
        join(model.Designation, model.Designation.id == model.User.designation_id).\
        join(model.Department, model.Department.id == model.User.department_id).\
        join(model.Project, model.Project.id == model.User.project_id).\
        add_columns(model.User.email, model.User.name, model.User.date_of_birth, model.User.contact_number,
                    model.Project.project_name, model.Designation.designation_name, model.Department.department_name).\
        filter(model.User.report_to == session['id'])

    return render_template('dashboard/team.html', member_set=member_set)


@dashboardAPI.route('/dashboard/delete/<int:tab>/<int:delete_id>', methods=['GET'])
@login_required
@required_roles('super', 'admin')
def delete(delete_id, tab):

    if tab == 1:
        model.db.session.delete(model.Project.query.filter_by(id=delete_id).first())
        model.db.session.commit()

        flash('Project deleted', 'warning')
        return redirect(url_for('dashboardAPI.projects'))
    elif tab == 2:
        model.db.session.delete(model.Department.query.filter_by(id=delete_id).first())
        model.db.session.commit()

        flash('Department deleted', 'warning')
        return redirect(url_for('dashboardAPI.departments'))
    elif tab == 3:
        model.db.session.delete(model.Designation.query.filter_by(id=delete_id).first())
        model.db.session.commit()

        flash('Designation deleted', 'warning')
        return redirect(url_for('dashboardAPI.designations'))


@dashboardAPI.route('/search', methods=['GET', 'POST'])
@login_required
@required_roles('super', 'admin', 'staff')
def search():

    word = request.form['search']
    results = model.User.query.whoosh_search(word).all()

    return render_template('search.html', results=results)


@dashboardAPI.route('/dashboard/history/leaves')
@login_required
@required_roles('super', 'admin')
def leaves_all():
    leave_history = model.LeaveHistory.query. \
        join(model.LeaveType, model.LeaveHistory.leave_type_id == model.LeaveType.id). \
        join(model.User, model.LeaveHistory.employee_id == model.User.id). \
        add_columns(model.LeaveType.leave_type_name, model.LeaveHistory.applied_date, model.LeaveHistory.reason,
                    model.LeaveHistory.contact_number, model.LeaveHistory.starting_date,
                    model.LeaveHistory.number_of_days, model.LeaveHistory.acting_arrangements,
                    model.LeaveHistory.leave_status_id, model.LeaveHistory.id, model.User.name,
                    model.User.employee_number, model.User.company_number). \
        filter(model.LeaveHistory.leave_status_id != 4).all()

    return render_template('dashboard/leaves.html', leave_history=leave_history)


@dashboardAPI.route('/dashboard/history/work', methods=['POST', 'GET'])
@login_required
@required_roles('super', 'admin')
def work_all():
    leave_balance = model.WorkHoliday.query. \
        join(model.User, model.User.id == model.WorkHoliday.employee_id). \
        add_columns(model.WorkHoliday.applied_date, model.WorkHoliday.date, model.WorkHoliday.leave_status_id,
                    model.WorkHoliday.id, model.User.name, model.User.employee_number, model.User.company_number,
                    model.WorkHoliday.approved_person_name).\
        filter(model.WorkHoliday.leave_status_id != 4).all()

    return render_template('dashboard/work.html', work_history=leave_balance)


@dashboardAPI.route('/dashboard/report', methods=['POST', 'GET'])
@login_required
@required_roles('super', 'admin')
def report():
    form = Report(request.form)

    if request.method == 'POST' and form.validate():
        start_date = form.start_date.data
        end_date = form.end_date.data

        leave_data = model.LeaveHistory.query. \
            join(model.LeaveType, model.LeaveHistory.leave_type_id == model.LeaveType.id). \
            join(model.User, model.LeaveHistory.employee_id == model.User.id). \
            add_columns(model.LeaveType.leave_type_name, model.LeaveHistory.applied_date,
                        model.LeaveHistory.starting_date, model.LeaveHistory.number_of_days, model.User.name,
                        model.User.employee_number, model.User.company_number). \
            filter(model.LeaveHistory.starting_date <= end_date). \
            filter(model.LeaveHistory.starting_date >= start_date). \
            filter(model.LeaveHistory.leave_status_id == 2). \
            all()

        column_names = ['name', 'employee_number', 'company_number', 'starting_date', 'number_of_days']

        return excel.make_response_from_query_sets(leave_data, column_names, "xlsx")

    return render_template('dashboard/report.html', form=form)
