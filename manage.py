from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from app import app
from model import db

app.config.from_pyfile('config.py')

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)


@manager.command
def seed():
    import model

    db.session.add_all([model.Role(name='Super User'),
                        model.Role(name='Admin'),
                        model.Role(name='Staff'),
                        model.Role(name='Employee')])
    db.session.flush()

    db.session.add(model.Project(project_name=None))
    db.session.flush()

    db.session.add(model.Designation(designation_name=None))
    db.session.flush()

    db.session.add(model.Department(department_name=None))
    db.session.flush()

    db.session.add(model.User(email='admin',
                              name='Admin',
                              role_id=1,
                              password='$5$rounds=535000$tpF2x5W42cm1v4.K$YxDqu6MMUh7ydRsrnLVRKSSMY3cjg0nFurqa3a00z7A'))
    db.session.flush()

    db.session.add_all([model.LeaveStatus(status='Pending Approval'),
                        model.LeaveStatus(status='Approved'),
                        model.LeaveStatus(status='Disapproved'),
                        model.LeaveStatus(status='Delete')])
    db.session.flush()

    db.session.add_all([model.LeaveType(leave_type_name='Annual Leave', visibility=True),
                        model.LeaveType(leave_type_name='Medical Leave', visibility=True),
                        model.LeaveType(leave_type_name='Casual Leave', visibility=True),
                        model.LeaveType(leave_type_name='No Pay Leave', visibility=False),
                        model.LeaveType(leave_type_name='Maternity Leave', visibility=False),
                        model.LeaveType(leave_type_name='Lieu Leave', visibility=True)])
    db.session.flush()

    db.session.add_all([model.Month(month='January'),
                        model.Month(month='February'),
                        model.Month(month='March'),
                        model.Month(month='April'),
                        model.Month(month='May'),
                        model.Month(month='June'),
                        model.Month(month='July'),
                        model.Month(month='August'),
                        model.Month(month='September'),
                        model.Month(month='October'),
                        model.Month(month='November'),
                        model.Month(month='December')])
    db.session.flush()

    db.session.add_all([model.Year(year='2018'),
                        model.Year(year='2019')])

    db.session.commit()


@manager.command
def salary():
    import model

    users = model.User.query.filter(model.User.company_number is not None)

    for user in users:
        salary_table = model.Salary(month_fk=11, year_fk=1, user_fk=user.id, pdf=user.employee_number+'.pdf')
        model.db.session.add(salary_table)
        model.db.session.commit()


if __name__ == '__main__':
    manager.run()
