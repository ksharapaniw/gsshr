from wtforms import Form, StringField, validators, DateField, FloatField, PasswordField


class AddProject(Form):
    project_name = StringField('Project Name', [validators.required(), validators.length(max=200)])


class AddDesignation(Form):
    designation_name = StringField('Designation', [validators.required(), validators.length(max=200)])


class AddDepartment(Form):
    department_name = StringField('Department', [validators.required(), validators.length(max=200)])


class RegisterEmployee(Form):
    email = StringField('Email', [validators.email(message=None)])
    name = StringField('Name', [validators.length(min=5, max=200)])
    date_of_birth = DateField('Date of Birth')
    company_number = StringField('Company Number')
    employee_number = StringField('Employee Number')
    nic = StringField('NIC', [validators.length(min=10, max=12)])
    contact_number = StringField('Contact Number')
    emergency_contact_number = StringField('Emergency Contact Number')
    address = StringField('Address')
    date_of_hire = DateField('Date of Hire')
    date_of_join = DateField('Date of Join')


class ApplyLeave(Form):
    reason = StringField('Reason', [validators.required()])
    contact_number = StringField('Contact Number', [validators.length(min=10, max=10)])
    number_of_days = FloatField('Number of Days', [validators.NumberRange(min=0.5, max=14)])
    acting_arrangements = StringField('Acting Arrangements')
    start_date = DateField('Starting Date')


class WorkHoliday(Form):
    date = DateField('Working Date')


class ChangePassword(Form):
    current_password = PasswordField('Current Password', [validators.DataRequired()])
    new_password = PasswordField('New Password', [validators.DataRequired(),
                                                  validators.EqualTo('confirm', message='Password do not match')])
    confirm = PasswordField('Confirm Password', [validators.DataRequired()])


class Search(Form):
    search = StringField('Search', [validators.required()],
                         render_kw={"placeholder": "Email, Name, Company Number, Employee Number, NIC"})


class Report(Form):
    start_date = DateField('Start Date')
    end_date = DateField('End Date')
