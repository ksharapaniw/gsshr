from flask import Flask, session
from flask_mail import Mail
from flask_bootstrap import Bootstrap
from flask_admin import Admin
from flask_login import LoginManager
import os
import flask_excel as excel

__author__ = 'ksharapani'

app = Flask(__name__, instance_path='/salary')
app.config.from_pyfile('config.py')
import model
admin = Admin(app, name='My GSS', template_mode='bootstrap3')
mail = Mail(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'userAPI.login'
Bootstrap(app)
excel.init_excel(app)


APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER = os.path.join(APP_ROOT, 'uploads')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

from API_modules import DashboardAPI
from API_modules import LeaveAPI
from API_modules import UserAPI
from API_modules import SalaryAPI
app.register_blueprint(DashboardAPI.dashboardAPI)
app.register_blueprint(LeaveAPI.leaveAPI)
app.register_blueprint(UserAPI.userAPI)
app.register_blueprint(SalaryAPI.salaryAPI)


admin.add_view(model.EmployeeView(model.User, model.db.session))
admin.add_view(model.AdminView(model.Project, model.db.session))
admin.add_view(model.AdminView(model.Department, model.db.session))
admin.add_view(model.AdminView(model.Designation, model.db.session))
admin.add_view(model.AdminView(model.LeaveBalance, model.db.session))
admin.add_view(model.AdminView(model.Role, model.db.session))
admin.add_view(model.SuperView(model.LeaveHistory, model.db.session))
admin.add_view(model.SuperView(model.LeaveStatus, model.db.session))
admin.add_view(model.SuperView(model.WorkHoliday, model.db.session))
admin.add_view(model.SuperView(model.Month, model.db.session))
admin.add_view(model.SuperView(model.Year, model.db.session))
admin.add_view(model.SuperView(model.Salary, model.db.session))


@login_manager.user_loader
def user_load(email):
    return model.User.query.get(email)


from datetime import timedelta


@app.before_request
def make_session_permanent():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(minutes=20)
