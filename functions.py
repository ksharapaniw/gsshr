from functools import wraps, update_wrapper
from flask.helpers import flash
from flask import redirect, url_for, session, make_response
from app import mail
from flask_mail import Message


def required_roles(*roles):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            if session.get('role') not in roles:
                flash('Authentication error, please check your details and try again', 'danger')
                return redirect(url_for('userAPI.index'))
            return f(*args, **kwargs)
        return wrapped
    return wrapper


def email(subject, recipients, html):
    try:
        msg = Message(subject,
                      sender='noreply@gssintl.biz',
                      recipients=[recipients],
                      html=html)
        mail.send(msg)
        return True

    except Exception as e:
        flash(str(e), 'warning')
        return False

